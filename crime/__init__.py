import urllib
import json


class CrimeImp:
    def __init__(self, offense, key):
        urllib.request.urlcleanup()
        self.response = urllib.request.urlopen(
            urllib.request.Request(
                f"https://api.usa.gov/crime/fbi/sapi/api/data/nibrs/{offense}/offense/national/count?api_key={key}"
            )
        ).read()
        self.obj = json.loads(self.response)

    def results(self):
        return self.obj['results']

    def by_year(self):
        return {d["data_year"]: d for d in self.results()}

    def __str__(self, year):
        return str(year) + ": " + str(self.by_year()[year]["incident_count"])
