# crime
FBI crime API wrapper

## Very Special Thanks

[![CloudRepo Logo](https://www.cloudrepo.io/assets/img/logo/landscape/CloudRepo-Landscape-Brand-Blue.png)](https://cloudrepo.io)

The team would like to thank CloudRepo for their legendary repositories.

CloudRepo proudly sponsors open source projects with cloud-based, fully managed repositories. Take a look at CloudRepo's fully managed [Maven Repositories](https://www.cloudrepo.io/private-maven-repositories.html) and [Python Repositories](https://www.cloudrepo.io/pip.html). By purchasing any plan from them, you help out Cakebot and CloudRepo.
